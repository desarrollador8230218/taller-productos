import { Component } from '@angular/core';
import { IonHeader, IonToolbar, IonTitle, IonContent, IonLabel, IonList, IonItem, IonCard, IonInput, IonSpinner, IonButtons, IonButton, IonIcon, IonImg, IonCardHeader, IonCardTitle, IonCardContent, IonRow, IonGrid, IonCol } from '@ionic/angular/standalone';
import { UserI } from '../common/models/users.models';
import { FirestoreService } from '../common/services/firestore.service';
import { FormsModule } from '@angular/forms';
import { IoniconsModule } from '../common/modules/ionicons.module';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
  standalone: true,
  imports: [IonCol, IonGrid, IonRow, IonCardContent, IonCardTitle, IonCardHeader, IonImg, IonList, IonLabel, IonHeader, IonToolbar, IonTitle, IonContent, IonList, IonItem, IonInput,
    IonIcon, IonButton, IonButtons, IonSpinner, IonInput, IonCard, 
    FormsModule,
    IoniconsModule
  ],
})
export class HomePage {

  users: UserI[] = [];

  newUser: UserI;
  cargando: boolean = false;

  user: UserI


  constructor(private firestoreService: FirestoreService) {
    this.loadusers();
    this.initUser();
    this.getuser();
  }

  loadusers() {
    this.firestoreService.getCollectionChanges<UserI>('Productos').subscribe( data => {
      if (data) {
        this.users = data
      }

    })

  }

  initUser() {
    this.newUser = {
      presupuesto: null,
      unidad: null,
      producto: null,
      cantidad: null,
      valorunitario: null,
      valortotal: null,
      fechadquisicion: null,
      proveedor: null,
      id: this.firestoreService.createIdDoc(),
    }
  }

  async save() {
    this.cargando = true;
    await this.firestoreService.createDocumentID(this.newUser, 'Productos', this.newUser.id)
    this.cargando = false;
  }

  edit(user: UserI) {
    console.log('edit -> ', user);
    this.newUser = user;
  }

  async delete(user: UserI) {
    this.cargando = true;
    await this.firestoreService.deleteDocumentID('Productos', user.id);
    this.cargando = false;
  }

  async getuser() {
    const uid = '82304af9-d177-49c1-b07e-f443cf5bb3c1';
     this.firestoreService.getDocumentChanges<UserI>('Productos/' + uid).subscribe( data => {
       console.log('getuser -> ', data);
       if (data) {
         this.user = data
       }
     })

    const res = await this.firestoreService.getDocument<UserI>('Productos/' + uid);
    this.user = res.data()
  }


}
