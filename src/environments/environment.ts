// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCDrlKWHYdmjGQprqeRTqTXNM4PXGVpRqo",
    authDomain: "taller-productos-ionic.firebaseapp.com",
    projectId: "taller-productos-ionic",
    storageBucket: "taller-productos-ionic.appspot.com",
    messagingSenderId: "690551413111",
    appId: "1:690551413111:web:3638ad497109c972b36972",
    measurementId: "G-4XK0865GF3"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
